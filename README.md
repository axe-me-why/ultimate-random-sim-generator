# Sim Generator

## Introduction
This application generates random sims for your chosen "The Sims" game. What sets it apart from other sim generators, though, is its _configurability_. Everything about how this generator works, including which traits can be generated (or whether traits are generated at all) to whether basic demographic factors are used as weights in randomly determining aspects of your sim's personality ***CAN ALL BE CUSTOMIZED***. 

If that doesn't blow your mind, let me put it another way:

You know how there are random sim generators out there that allow you to disable certain traits? That's configurability. Well, this generator takes that configurability to the extreme. This generator is so configurable that you can actually use it to generate a random sim for any "The Sims" game! That's right, it's not limited to just The Sims 4 - it can be customized so heavily that you can use it for the Sims 2 just as easily as for The Sims 3 or The Sims 4, despite that your sim's personality in each of those games is determined by completely different factors.

***IN FACT***, this generator is so gosh darn configurable that you can use it to randomize things that aren't even remotely related to The Sims!

So how does it work? When you load the application up, there will be a big lovely button that says "load configuration". Push that button. You will be prompted to select a .json configuration file. A sample .json file comes packaged with this application, but you can create your own (and are encouraged to do so!). This configuration file tells the application what "categories" of attributes need to be randomized, what the options are for each category, and how heavily each option in that category is. It can also define "settings" that are defined by the user - these settings will essentially "seed" the random generation. You can think of this configuration file as a list of instructions where you say things like, "Choose 3 traits from this list of 30 traits by rolling a 30-sided die three times. Assign the following numbers to each trait, and if that number is rolled that trait is chosen".

The following sections will detail how to set up this configuration file.

## Seed Settings
The first section we'll talk about is the "seed settings" section, which is indicated in the JSON file as `seedSettings`. The items that you define here will be options that are chosen manually by the user rather than being randomly generated. These options can then be used to determine which things should be randomly generated, and they can also be used to apply different weights to different options depending on what the user has chosen.

An example of what you can do with this settings can be found in the sample JSON file. Perhaps you want the parents' traits to be taken into account when determining the random sim's traits. To do that, you can use `seedSettings` to give the user text fields to enter in each parents traits, and those traits will then be used by the random generator to determine the new sim's traits.

You could also decide that men are more likely to get the "hot-headed" trait than women. So you could include an option in `seedSettings` that allows the user to choose the sex/gender of the sim to be generated, and apply a greater or lesser weight for "hot-headed" for the generated sim.

Here is an example "seedSettings" section:

```json
{
    "name": "Sex",
    "type": "select",
    "options": ["Female", "Male"],
    "default": "Female"
},
{
    "name": "Race",
    "type": "select",
    "options": ["Black", "Caucasian", "Asian", "Hispanic"],
    "default": "Black"
},
{
    "name": "Mother traits",
    "type": "textFieldGroup",
    "numberOfFields": 5
},
{
    "name": "Father traits",
    "type": "textFieldGroup",
    "numberOfFields": 5
}
```

The "name" field is required and must be unique. The "type" field is also required, and has two options: "textFieldGroup" and "select".

If "type" is set to "select", you must also provide the "options" field and the "default" field. What the user sees is a "select list" (also known as a "dropdown") control that allows them to select from several options. The options they can select from is defined in "options".

If "type" is set to "select", "numberOfFields" must also be provided. This type of seed setting provides the user with a text field. The user can type into this field, and each time they enter a semicolon (";") the text they've typed in will be "entered" as a value and they'll be able to enter an additional value, until they've hit the maximum specified in "numberOfFields". The items they've entered will be displayed below the text field, and will include "x" buttons that allow the user to delete the individual items if they're not happy with them.

## Categories

Categories are the other main component in a sim generator config file. Each category represents an attribute or "thing" about your sim that you want to randomize. For example, if you want your sims traits to be chosen from a list of 30 possible traits, that's a category! If you want your sim's aspiration to be randomized, that's another category! If we're talking about The Sims 2 and you want your sim's Outgoing/Introverted attribute to be randomized, well, that can be a category too.

Here's an example of a very simple category:

```json
{ 
  "name": "Aspiration", 
  "items": [
    { "name": "Angling Ace" },
    { "name": "Beach Life"},
    { "name": "Bestselling Author"},
    { "name": "Bodybuilder"},
    { "name": "Business Savvy" },
    { "name": "Master Actor"},
    { "name": "Musical Genius"},
    { "name": "Quick Learner" }
  ] 
},
```
This demonstrates a category's two basic properties: "name" and "items". Both are required. "name" must be unique, and "items" should include one or more objects. Each of those objects must have their own "name" property that must be unique within that category.

In the above example, we're randomizing the sim's aspiration, and giving it 8 items. Since we don't define a weight for any of them, they all have an equal probability of being chosen, so it's like rolling an 8-sided die where each of those options has been assigned a number on that die.

Let's look at a _slightly_ more complex example:

```json
{
    "name": "Non-inherited Traits",
    "choose": 3,
    "items": [
        { "name": "Active", "weight": 1 },
        { "name": "Ambitious", "weight": 2 },
        { "name": "Art Lover", "weight": 1 },
        { "name": "Bookworm", "weight": 1 },
        ...
        { "name": "Vegetarian", "weight": 5 }
    ]
}
```

In the above example, we're randomizing a sim's traits. For the "name" I've gone with "non-inherited traits", and for the items I've utilized a new property: "weight". I can put a weight on any given item that gives that item a higher or lower probability of being chosen. This number must be an integer greater than zero, by the way - no fractions or decimals or zeroes or negative numbers here. Here, I've given "Ambitious" a weight of 2, so it's twice as likely to be chosen as something like "Active". I've given "Vegetarian" a massive weight of 5, which means it's five times more likely to be chosen than "Active"!

We're also utilizing another property on the category itself, called "choose". This property (which is optional) tells the application how many items to choose when generating the sim. In this case, I'm telling it to generate 3 traits from the list I've defined. If you do not specify this property, the default is 1.

Weights can be a little more complex, by the way. Take a look at the following example:

```json
    {
      "name": "Sexual Orientation",
      "items": [
        {"name": "Dates opposite gender", "weight": [
          ["Sex:Male", 5],
          ["Sex:Female", 2]
        ]}, {
          "name": "Bisexual",
          "weight": 1
        }, {
          "name": "Dates same gender",
          "weight": 1
        }
      ]
    },
```

Here, I'm telling the generator to generate a sexual orientation for my sim, and giving it three options: "Dates opposite gender", "Bisexual", and "Dates same gender". I've given "Bisexual" and "dates same gender" a simple weight of 1, but for the "Dates opposite gender" option, the weight depends on one of my seed settings! Specifically, I have a seed setting called "Sex" with the possible values of "Male" and "Female". These are specified by putting them in the same string of text together, separated by a colon (":") and no spaces.

So in that above example, I've specified that I want women to be twice as likely to be "dates opposite gender" as they are to be "bisexual", but men on the other hand to be 5 times as likely. Not everyone would like these settings - some of you may prefer for bisexual or gay sims to be more likely than "straight" sims, while other folks may prefer to not include gay or bi sims in their random sim generator at all. And that's the beauty of this level of customization - you can tailor it to your own sensibilities.

Now... let's take a look at _one more_ example:

```json
    {
      "name": "Endowment",
      "only": "Sex:Male",
      "items": [
        {
          "name": "Small",
          "weight": 2
        },
        {
          "name": "Normal",
          "weight": 2
        },
        {
          "name": "Big",
          "weight": 1
        },
        {
          "name": "Extra Big",
          "weight": 1
        },
        {
          "name": "Huge",
          "weight": 1
        }
      ]
    }
```

In a hypothetical world where someone uses WickedWhims or a similar adult-oriented mod, you might want to randomize a male sim's, um... _size_. You may also want to have this only be generated for male sims and not for female sims (you may also want it to be generated for both sexes, in which case, you do you, boo). 

Anyway, to achieve this we can use the "only" property for a category. Here I'm specifying "Sex:Male" - "Sex" is the name of the seed setting I want the generator to look at, and "Male" is the only option which will allow this category to be used. The end result is that this category won't even appear on the final output of the application if I chose "Female" for "Sex".