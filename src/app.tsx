import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { MainPage } from './components/mainPage';

ReactDOM.render(<MainPage/>, document.getElementById('root'));