import React from "react";
import { SeedSetting, SeedSettingTextFieldGroup } from "../interfaces/config";
import { FormGroup, RadioGroup, FormControlLabel, Radio, TextField } from "@material-ui/core";
import { TextFieldGroupSetting } from "./textFieldGroupSetting";

export const SettingsGroup = ({
  s,
  value,
  setValue
}: {
  s: SeedSetting;
  value: string;
  setValue: (name: string, value: string) => void;
}) => {
  return (
    <section key={s.name}>
      <header>{s.name}</header>
      {s.type === 'select' ?
        <FormGroup row>
          <RadioGroup value={value} row onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setValue(s.name, e.target.value)
          }}>
            {s.options.map(o => (
              <FormControlLabel key={o} value={o} control={<Radio />} label={o} />
            ))}
          </RadioGroup>
        </FormGroup>
        : null
      }
      {s.type === 'textFieldGroup' ?
        <TextFieldGroupSetting
          s={s as SeedSettingTextFieldGroup}
          {...{setValue, value}}
        />
      : null}
    </section>
  );
};
