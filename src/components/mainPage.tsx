import React, { useEffect, useState, useMemo } from 'react';
import {readFileSync} from 'fs';
import linq from 'linq';
import { Config } from '../interfaces/config';
import { Sim } from '../interfaces/sim';
import { generateSim } from '../generation/generateSim';
import { SettingsGroup } from './settingsGroup';
import {remote} from 'electron'
import { isArray } from 'util';
import {Button, createMuiTheme, Container, Card, CardContent} from '@material-ui/core';
import { ThemeProvider, makeStyles } from '@material-ui/styles';

import './mainPage.css';

const theme = createMuiTheme();

export const MainPage = () => {
  const classes = useStyles();
  const [filePath, setFilePath] = useState<string | null>(null);
  const [data, setData] = useState<Config>();
  const [sim, setSim] = useState<Sim>();
  const [settings, setSettings] = useState<
    { setting: string; value: string }[]
  >([]);
  const showFileSelectDialog = () => {;
    const openDialogResult = remote.dialog.showOpenDialog({ properties: ['openFile'] }) as Promise<any> | string[]
    console.log(openDialogResult);
    if(isArray(openDialogResult)) {
      setFilePath(openDialogResult[0])
    }
    else {openDialogResult.then(f => {
      console.log(f)
      if(f.canceled) {
        return;
      }
      console.log(`file paths ${f.filePaths}`)
      setFilePath(f.filePaths[0])
    })}
  }
  useEffect(() => {
      if(!filePath) {
        return;
      }
      const fileContents = readFileSync(filePath, {encoding: 'utf8'})
      const config = JSON.parse(fileContents) as Config;
      setData(config);
      setSettings(
        config.seedSettings.map(s => ({setting: s.name, value: s.type === 'select' ? s.default : ''}))
      )
  }, [filePath]);
  const settingsWithValues = useMemo(
    () =>
      data
        ? linq
            .from(data.seedSettings)
            .join(
              settings,
              d => d.name,
              s => s.setting,
              (d, s) => ({ setting: d, value: s.value })
            )
            .toArray()
        : [],
    [settings, data]
  );
  const setValue = (name: string, value: string) => {
    setSettings(s => [
      ...s.filter(a => a.setting !== name),
      { setting: name, value }
    ]);
  };
  return (
    <ThemeProvider theme={theme}>
      <Container>
        <Button onClick={showFileSelectDialog} variant="contained" color="primary">Load Configuration</Button>
        <h1>Sims Generator</h1>
        {data ? (
          <section>
            {settingsWithValues.map(s => (
              <SettingsGroup {...{ s: s.setting, value: s.value, setValue }} />
            ))}
          </section>
        ) : null}
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            if (!data) {
              return;
            }
            setSim(
              generateSim(
                data,
                settingsWithValues
                //settings.map(s => ({ name: s.setting, value: s.value }))
              )
            );
          }}
        >
          Generate
        </Button>
        {sim ? (
          <section>
            <h2>Your Sim</h2>
            {sim.attributes.map(a => (
              <Card key={a.name} className={classes.categoryCard}>
                <CardContent>
                  <h3>{a.name}</h3>
                    <ul>
                      {a.values.map(v => (
                        <li key={v}>{v}</li>
                      ))}
                    </ul>
                </CardContent>
              </Card>
            ))}
          </section>
        ) : null}
      </Container>
    </ThemeProvider>
  );
}

const useStyles = makeStyles({
  categoryCard: {
    marginBottom: '20px'
  }
})