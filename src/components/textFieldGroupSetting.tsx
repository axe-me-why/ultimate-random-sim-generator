import { SeedSettingTextFieldGroup } from "../interfaces/config";
import React, { useState } from "react";
import { FormGroup, TextField, Paper, Chip } from "@material-ui/core";

export const TextFieldGroupSetting = ({
  s,
  setValue,
  value
}: {
  s: SeedSettingTextFieldGroup;
  value: string;
  setValue: (name: string, value: string) => void;
}) => {
    const [inputValue, setInputValue] = useState('');
    const [values, setValues] = useState<string[]>([]);
    const addValue = (val: string) => {
      const newValuesArray = [...values, val.replace(';', '')]
      setValues(v => newValuesArray)
      setValue(s.name, newValuesArray.join(';'));
      setInputValue('');
    }
    const deleteItem = (val: string) => setValues(v => v.filter(i => i !== val))
  return (
    <>
        <FormGroup>
          <TextField
            value={inputValue}
            onChange={(e) => {
              const newVal = e.currentTarget.value
              setInputValue(newVal)
              if(newVal.endsWith(';')) {
                addValue(newVal)
              }
            }}
            variant="outlined"
            color="primary"
          />
          <Paper>
            {values.map(v => (
              <Chip label={v} onDelete={() => deleteItem(v)} />
            ))}
          </Paper>
        </FormGroup>
    </>
  );
};
