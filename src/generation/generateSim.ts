import { Sim } from "../interfaces/sim";
import * as linq from "linq";
import { Config, SeedSetting } from "../interfaces/config";
import { isArray } from "util";

export const generateSim = (
  configData: Config,
  settings: {setting: SeedSetting; value: string;}[]
) => {
  if (!configData) {
    return undefined;
  }
  const finalSim: Sim = {
    attributes: []
  };
  const basicSettings = settings.filter(s => s.setting.type === 'select');
  basicSettings.forEach(s => {
    if(s.setting.type !== 'select') {
      return;
    }
    finalSim.attributes.push({name: s.setting.name, values: [s.value]})
  })
  const textFieldGroupSettings = settings.filter(s => s.setting.type === 'textFieldGroup');
  textFieldGroupSettings.forEach(s => {
    const setting = s.setting;
    if(setting.type !== 'textFieldGroup' || !s.value){
      return;
    }
    const options = s.value.split(';').filter(o => !!o);
    if(!options.length){
      return;
    }
    let chosenIndex = Math.round(Math.random() * options.length);
    if(chosenIndex >= options.length) {
      chosenIndex = options.length - 1;
    }
    const chosenOption = options[chosenIndex];
    finalSim.attributes.push({name: setting.name, values: [chosenOption]})
  })
  configData.categories.forEach(d => {
    if (d.only) {
      const settingName = d.only.split(":")[0];
      const value = d.only.split(":")[1];
      const selectedValue = linq
        .from(settings)
        .firstOrDefault(s => s.setting.name === settingName).value;
      if (value !== selectedValue) {
        return;
      }
    }
    const optionsSpreadOut = linq
      .from(d.items)
      .where(
        i =>
          !i.only ||
          linq
            .from(i.only.values)
            .any(
              v =>
                v ===
                linq
                  .from(settings)
                  .firstOrDefault(s => s.setting.name === (i.only && i.only.setting))
                  .value
            )
      )
      .select(i => linq.range(0, (isArray(i.weight) ? (
        linq.from(i.weight)
          .join(settings, w => w[0], s => `${s.setting.name}:${s.value}`, (w, s) => w[1])
          .firstOrDefault() || 1
      ) : i.weight) || 1).select(n => i.name))
      .selectMany(i => i)
      .toArray();
    const totalWeight = optionsSpreadOut.length;
    //   const totalWeight = linq.from(d.items).sum(i => i.weight || 1);
    const selectedOptions = [] as string[];
    for (var x = 0; x < (d.choose || 1); x++) {
      let isAlreadyChosen = false;
      do {
        const randomNumber = Math.floor(Math.random() * totalWeight);
        const chosenOption = optionsSpreadOut[randomNumber];
        if (linq.from(selectedOptions).any(o => o === chosenOption)) {
          isAlreadyChosen = true;
        } else {
          selectedOptions.push(chosenOption);
          isAlreadyChosen = false;
        }
      } while (isAlreadyChosen);
    }
    finalSim.attributes.push({
      name: d.name,
      values: selectedOptions
    });
  });
  return finalSim;
};
