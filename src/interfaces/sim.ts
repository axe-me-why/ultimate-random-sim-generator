export interface Sim {
    attributes: Attribute[]
}

interface Attribute {
    name: string,
    values: string[]
}