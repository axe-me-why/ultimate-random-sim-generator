export interface ConfigCategory {
    name: string;
    choose?: number;
    only?: string;
    items: Item[];
  }
  
  interface Item {
    name: string;
    weight?: number | Array<[string, number]>;
    only?: {
      setting: string;
      values: string[];
    };
  }
  