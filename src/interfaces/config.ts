import { ConfigCategory } from "./configCategory";

export interface Config {
  categories: ConfigCategory[];
  seedSettings: SeedSetting[];
}

export type SeedSetting = SeedSettingSelect | SeedSettingTextFieldGroup;

export interface SeedSettingSelect {
  name: string;
  type: 'select'
  options: string[];
  default: string;
}

export interface SeedSettingTextFieldGroup {
  name: string;
  type: 'textFieldGroup',
  numberOfFields: number
}